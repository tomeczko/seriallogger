﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SerialLogger
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool loggingInProgress = false;
        private System.IO.Ports.SerialPort mSerialPort;
        private string mBuffer;
        private System.IO.StreamWriter mStreamWriter;
        private bool mWriteToLog = false;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void log( string _log )
        {
            this.Dispatcher.Invoke((Action)(() =>
                {
                    string timestampString = DateTime.Now.ToString("yyyy-mm-dd HH:mm:ss.ffff");
                    activityLog.Text += timestampString + " " + _log + "\n";
                    activityLog.ScrollToEnd();
                }));
        }

        private void fillInCombos()
        {
            baudrateCombobox.Items.Add("1200");
            baudrateCombobox.Items.Add("2400");
            baudrateCombobox.Items.Add("4800");
            baudrateCombobox.Items.Add("9600");
            baudrateCombobox.Items.Add("19200");
            baudrateCombobox.Items.Add("38400");
            baudrateCombobox.Items.Add("57600");
            baudrateCombobox.Items.Add("115200");
            parityCombobox.Items.Add("None");
            parityCombobox.Items.Add("Odd");
            parityCombobox.Items.Add("Even");
            parityCombobox.Items.Add("Mark");
            parityCombobox.Items.Add("Space");
            stopBitsCombobox.Items.Add("0");
            stopBitsCombobox.Items.Add("1");
            stopBitsCombobox.Items.Add("1.5");
            stopBitsCombobox.Items.Add("2");

            // Setting defaults
            baudrateCombobox.SelectedIndex = 3;
            parityCombobox.SelectedIndex = 0;
            stopBitsCombobox.SelectedIndex = 1;

            // Let's find serial ports
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            foreach (var portName in ports)
                serialPortCombobox.Items.Add(portName);
            if (serialPortCombobox.Items.Count > 0)
                serialPortCombobox.SelectedIndex = 0;
        }

        private void MainWindow1_Loaded(object sender, RoutedEventArgs e)
        {
            log("Application started. ");
            log("Scanning for serial ports...");
            fillInCombos();
        }

        private void loggingProcess( bool _newState, string _filename )
        {
            if( _newState == true )
            {
                // Let's check if the file exists
                if (System.IO.File.Exists(_filename) == true)
                {
                    var answer = MessageBox.Show("File " + _filename + " already exists. Overwrite?",
                        "Question", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if( answer == MessageBoxResult.No )
                        return;
                }

                // Let's check if we can open the serial port
                string serialPortname = serialPortCombobox.SelectedItem.ToString();
                int baudrate = int.Parse(baudrateCombobox.SelectedItem.ToString());
                System.IO.Ports.Parity parity = System.IO.Ports.Parity.None;
                if (parityCombobox.SelectedIndex == 0) parity = System.IO.Ports.Parity.None;
                if (parityCombobox.SelectedIndex == 1) parity = System.IO.Ports.Parity.Odd;
                if (parityCombobox.SelectedIndex == 2) parity = System.IO.Ports.Parity.Even;
                if (parityCombobox.SelectedIndex == 3) parity = System.IO.Ports.Parity.Mark;
                if (parityCombobox.SelectedIndex == 4) parity = System.IO.Ports.Parity.Space;
                System.IO.Ports.StopBits stopbits = System.IO.Ports.StopBits.None;
                if (stopBitsCombobox.SelectedIndex == 0) stopbits = System.IO.Ports.StopBits.None;
                if (stopBitsCombobox.SelectedIndex == 1) stopbits = System.IO.Ports.StopBits.One;
                if (stopBitsCombobox.SelectedIndex == 2) stopbits = System.IO.Ports.StopBits.OnePointFive;
                if (stopBitsCombobox.SelectedIndex == 3) stopbits = System.IO.Ports.StopBits.Two;
                mSerialPort = new System.IO.Ports.SerialPort(serialPortname, baudrate, parity, 8, stopbits)
                {
                   Handshake = System.IO.Ports.Handshake.None,
                   ReadTimeout = 500,
                   WriteTimeout = 500
                };
                mSerialPort.DataReceived += mSerialPort_DataReceived;
                mSerialPort.Open();

                // Now continue with opening a file
                try
                {
                    mStreamWriter = new System.IO.StreamWriter(_filename);
                }
                catch
                {
                    MessageBox.Show("Failed to open log file!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                
                serialPortCombobox.IsEnabled = false;
                baudrateCombobox.IsEnabled = false;
                parityCombobox.IsEnabled = false;
                stopBitsCombobox.IsEnabled = false;
                logFilenameTextbox.IsEnabled = false;
                logLocationBtn.IsEnabled = false;
                startStopBtn.Content = "STOP";
                loggingInProgress = true;
            }
            else
            {
                mSerialPort.Close();
                if (mStreamWriter != null)
                    mStreamWriter.Close();
                while (mSerialPort.IsOpen) ;
                serialPortCombobox.IsEnabled = true;
                baudrateCombobox.IsEnabled = true;
                parityCombobox.IsEnabled = true;
                stopBitsCombobox.IsEnabled = true;
                logFilenameTextbox.IsEnabled = true;
                logLocationBtn.IsEnabled = true;
                startStopBtn.Content = "START";
                loggingInProgress = false;
            }
        }

        void mSerialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            System.IO.Ports.SerialPort sp = (System.IO.Ports.SerialPort)sender;
            string indata = sp.ReadExisting();
            string result = System.Text.RegularExpressions.Regex.Replace(indata, @"\r\n?|\n", "@");
            mBuffer += indata;
            List<string> split = new List<string>();
            while (mBuffer.IndexOf("\n") != -1)
            {
                int splitPoint = mBuffer.IndexOf("\n");
                split.Add(mBuffer.Substring(0, splitPoint));
                mBuffer = mBuffer.Remove( 0, splitPoint+1);
            }

            foreach (var line in split)
            {
                if (mStreamWriter != null)
                    mStreamWriter.WriteLine(line);
                this.Dispatcher.Invoke((Action)(() =>
                    {
                        if (logToConsoleCheckbox.IsChecked == true)
                            log("LOG: " + line);
                    }));
            }
            if (mStreamWriter != null)
                mStreamWriter.Flush();
        }

        private void startStopBtn_Click(object sender, RoutedEventArgs e)
        {
            if( loggingInProgress )
            {
                log("Stopping logging...");
                loggingProcess(false, logFilenameTextbox.Text);
                log("Logging stopped.");
            }
            else
            {
                log("Starting logging...");
                loggingProcess(true, logFilenameTextbox.Text);
                log("Logging started.");
            }
        }

    }
}
